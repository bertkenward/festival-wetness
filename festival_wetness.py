"""
Determine how wet a Cambridge Beer Festival was

Uses data from the Cambridge University Computer Lab weather station
"""
import collections
import datetime
import functools
import urllib.request
from typing import Iterator


@functools.cache
def get_opening_date(year: int) -> datetime.date:
    """
    Get the opening day of the festival for a given year
    """
    end_of_may = datetime.date(year, 5, 31)
    return datetime.date(year, 5, 25 - end_of_may.isoweekday())


@functools.lru_cache(maxsize=1)
def was_setup(date: datetime.date) -> bool:
    """
    Was the festival setting up on a given date?
    """
    opening_date = get_opening_date(date.year)
    if date >= opening_date:
        return False
    start_date = opening_date - datetime.timedelta(days=7)
    return date >= start_date


@functools.lru_cache(maxsize=1)
def was_open(date: datetime.date) -> bool:
    """
    Was the festival open on a given date?
    """
    opening_date = get_opening_date(date.year)
    if date < opening_date:
        return False
    closing_date = opening_date + datetime.timedelta(days=5)
    return date <= closing_date


@functools.lru_cache(maxsize=1)
def was_takedown(date: datetime.date) -> bool:
    """
    Was the festival taking down on a given date?
    """
    closing_date = get_opening_date(date.year) + datetime.timedelta(days=5)
    if date <= closing_date:
        return False
    finish_date = closing_date + datetime.timedelta(days=6)
    return date <= finish_date


def get_data() -> Iterator[tuple]:
    """
    Yield data from the CL weather station
    """
    with urllib.request.urlopen(
        "https://www.cl.cam.ac.uk/weather/weather-raw.csv"
    ) as response:
        if response.status != 200:
            raise RuntimeError("Failed to get weather data")

        for line in response.readlines():
            fields = line.decode("utf-8").split(",")
            yield (datetime.datetime.fromisoformat(fields[0]).date(), int(fields[8]))


def rainfall_by_festival() -> dict[int, collections.Counter]:
    """
    Sum rainfall by festival year
    """
    rainfall_by_year: dict[int, collections.Counter] = collections.defaultdict(
        lambda: collections.Counter(setup=0, open=0, takedown=0)
    )

    for date, rainfall in get_data():
        if not rainfall:
            continue

        if was_setup(date):
            rainfall_by_year[date.year].update(setup=rainfall)
        elif was_open(date):
            rainfall_by_year[date.year].update(open=rainfall)
        elif was_takedown(date):
            rainfall_by_year[date.year].update(takedown=rainfall)

    return rainfall_by_year


def output_summary(data: dict) -> None:
    """
    Output some useful summary data
    """
    worst_open_year = None
    worst_open_rainfall = -1

    worst_pre_close_year = None
    worst_pre_close_rainfall = -1

    worst_total_year = None
    worst_total_rainfall = -1

    for year, totals in data.items():
        setup_rain = totals["setup"]
        open_rain = totals["open"]
        takedown_rain = totals["takedown"]

        print(
            f"{year}: {setup_rain/100.0:>7.2f} {open_rain/100.0:>7.2f} {takedown_rain/100.0:>7.2f}"
        )

        if open_rain > worst_open_rainfall:
            worst_open_rainfall = open_rain
            worst_open_year = year

        if setup_rain + open_rain > worst_pre_close_rainfall:
            worst_pre_close_rainfall = setup_rain + open_rain
            worst_pre_close_year = year

        if setup_rain + open_rain + takedown_rain > worst_total_rainfall:
            worst_total_rainfall = setup_rain + open_rain + takedown_rain
            worst_total_year = year

    print()
    print(
        f"Wettest open week was in {worst_open_year} "
        f"({worst_open_rainfall/100.0:.2f} mm)"
    )
    print(
        f"Wettest setup/open was in {worst_pre_close_year} "
        f"({worst_pre_close_rainfall/100.0:.2f} mm)"
    )
    print(
        f"Wettest setup/open/takedown was in {worst_total_year} "
        f"({worst_total_rainfall/100.0:.2f} mm)"
    )


if __name__ == "__main__":
    output_summary(rainfall_by_festival())
